<?php

return [
    'id' => 'yii2',
    'basePath' => realpath(__DIR__ . '/../'),
    'bootstrap' => [
        'debug'
    ],
    'components' => [
        'urlManager' => ['enablePrettyUrl' => true, 'showScriptName' => false],
        'db' => require __DIR__ . '/db.php',
        'user' => ['identityClass' => 'app\models\UserIdentity', 'enableAutoLogin' => true],
        'request' => ['cookieValidationKey' => 'MySecret$']
    ],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ["*"]
        ]
    ]
];