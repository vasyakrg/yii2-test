<?php
namespace app\controllers;

use \yii\web\Controller;
use \Yii;

class SiteController extends Controller
{
    public function actionIndex()
    {
//        Yii::debug('Hello from action index', 'actionIndex');
//        Yii::warning('Warning tea');
//        \Yii::error('error test');

        return $this->render('index');
    }

    public function actionJoin()
    {
        return $this->render('join');
    }

    public function actionLogin()
    {
        return $this->render('login');
    }
}