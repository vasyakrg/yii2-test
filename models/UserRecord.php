<?php
namespace app\models;

use \yii\db\ActiveRecord;
use \Yii;
use \Faker\Factory;

class UserRecord extends ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    public function setTestUser()
    {
        $faker = Factory::create();

        $this->name = $faker->name('man');
        $this->email = $faker->email;
        $this->setPassword($faker->password(6,12));
        $this->status = $faker->randomDigit;
    }

    public static function existsEmail($email)
    {
        $count = static::find()->where(['email' => $email])->count();
        return $count > 0;
    }

    public static function findUserByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function setUserJoinForm(UserJoinForm $userJoinForm)
    {
        $this->name = $userJoinForm->name;
        $this->email = $userJoinForm->email;
        $this->setPassword($userJoinForm->password);
        $this->status = 1;
    }

    public function setPassword($password)
    {
//        Yii::beginProfile('hash', __METHOD__);
        //стоиомость шифрования (default 13), все что выше - 2х увеличивает время шифрования
        $cost = 13;
        $this->passhash = Yii::$app->security->generatePasswordHash($password, $cost);
        $this->authokey = Yii::$app->security->generateRandomString(100);
//        Yii::endProfile('hash', __METHOD__);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passhash);
    }
}