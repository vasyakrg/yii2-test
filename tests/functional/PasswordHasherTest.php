<?php

use Codeception\Test\Unit;
use app\models\UserRecord;
use \yii\base\Security;

class PasswordHasherTest extends Unit
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testPasswordIsHash()
    {
        //придумали тестовый пароль
        $my_password = 'qaws123';

        //создал тествую запись и сгенерил тестового юзера
        $userRecord_local = new UserRecord();
        $userRecord_local->setTestUser();

        //даю ему тестовый пароль сверху
        $userRecord_local->setPassword($my_password);
        $userRecord_local->save();

        //нахожу созданного юзера в базе по его айди
        $userRecord_found = UserRecord::findOne($userRecord_local->id);

        //смотрю что юзер взятый с базы это именно тот, которого я создал ранее
        $this->assertInstanceOf(get_class($userRecord_local), $userRecord_found);

        // подключаю модуль безопасности фреймворка
        $security = new Security();
        //смотрю, что пароль проверяется через шех
        $this->assertTrue($security->validatePassword($my_password, $userRecord_found->passhash));
    }

    public function testPasswordIsNotRehashed()
    {
        //придумали тестовый пароль
        $my_password = 'qaws123';

        //создал тествую запись и сгенерил тестового юзера
        $userRecord_local = new UserRecord();
        $userRecord_local->setTestUser();

        //даю ему тестовый пароль сверху
        $userRecord_local->setPassword($my_password);
        $userRecord_local->save();

        $first_hash = $userRecord_local->passhash;

        $userRecord_local->name .= mt_rand(1,9);
        $userRecord_local->save();

        $this->assertEquals($first_hash, $userRecord_local->passhash);

        $userRecord_found = UserRecord::findOne($userRecord_local->id);
        $this->assertEquals($first_hash, $userRecord_found->passhash);
    }
}