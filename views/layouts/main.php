<?php
use \yii\bootstrap\NavBar;
use \yii\bootstrap\Nav;

$this->beginPage(); ?>
<html>
<head>
<!--    <title>test yii2 site</title>-->
    <?php $this->head(); ?>
</head>

<body>
<?php $this->beginBody(); ?>
    <?php
        NavBar::begin([
            'brandLabel' => 'Amega',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => ['class' => 'navbar-default navbar-fixed-top']
                    ]);

        if (Yii::$app->user->isGuest)
        {
            $items = [
                ['label' => 'Join', 'url' => ['/user/join']],
                ['label' => 'Login', 'url' => ['/user/login']]
            ];
        }
        else
        {
            $items = [
                ['label' => Yii::$app->user->getIdentity()->name],
                ['label' => 'Logout', 'url' => ['/user/logout']]
            ];
        }

        echo Nav::widget([
           'options' => ['class' => 'navbar-nav navbar-rigth'],
           'items' => $items
        ]);

        NavBar::end();
    ?>

    <div class="container" style="margin-top: 50px"></div>
        <?= $content ?>
    </div>

    <?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>